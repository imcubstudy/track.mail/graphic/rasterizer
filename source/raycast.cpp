#include "WFObj.hpp"
#include "Raycast.hpp"

#define _NTHREADS 8

#define _VSCRES 900
#define _HSCRES 1440

sphere const sph[] =
{
    {vec3_t{0.f, 0.f, -1e4f},     1e4f,   vec3_t{0.3f, 1.f, 0.3f}},
    {vec3_t{0.f, 0.f, 0.3f},      0.5f,   vec3_t{1.f, 1.f, 1.f}}
};
int const sph_size = sizeof(sph) / sizeof(sphere);

X11color_t toX11color(const vec3_t& v)
{
    return X11color_t {
        (unsigned char)(v.z * 255),
        (unsigned char)(v.y * 255),
        (unsigned char)(v.x * 255),
        255
    };
}

struct MyShader final: public shader_t<vec2_t, X11color_t>
{
    camera cam;

    vs_out_t vs(const vs_in_t &v) noexcept override
    {
        return vs_out_t{
            vec4_t{-v.x, -v.y, 0.f, -0.9999f},
            v
        };
    }

    color_t fs(const fs_in_t &v) noexcept override
    {
        ray const r = cast_from_cam(cam, v);
        intersection const I = find_intersection(r, sph, sph_size);
        if(I.idx < 0)
            return toX11color(vec3_t{0.1f, 0.1f, 0.4f});

        vec3_t const color = sph[I.idx].color;
        vec3_t const pos = ray_vec(r, I.t);
        vec3_t light = (vec3_t{1.f, 1.f, 2.f}).norm();
        intersection const shadow = find_intersection({pos, light}, sph, sph_size);
        if(shadow.idx >= 0)
            return toX11color(color * 0.3f);

        vec3_t const norm = (pos - sph[I.idx].pos).norm();
        vec3_t const halfway = (light - r.dir).norm();

        float const NH = std::max(0.f, norm.dot(halfway));
        float spec = NH;
        for(int i = 0; i < 4; i++)
            spec *= spec;
        float const diff = std::max(0.f, norm.dot(light));
        return toX11color(color * (0.3f + 0.4f * diff + 0.3f * spec));
    }
};

struct MyInterpolator_t final: interpolator_t<MyShader::fs_in_t, MyShader::vs_out_t, fragment_t>
{
    fs_in_t operator()(const std::vector<vs_out_t>& vs_out, const std::vector<unsigned>& inds, const fragment_t& fr) noexcept override;
};

inline MyShader::fs_in_t MyInterpolator_t::operator()(const std::vector<MyInterpolator_t::vs_out_t>& vs_out,
                                             const std::vector<unsigned>& inds, const fragment_t& fr) noexcept
{
    const float b = fr.b;
    const float c = fr.c;
    const float a = 1 - b - c;

    const MyShader::vs_in_t v[3] = {
        vs_out[inds[fr.id + 0]].vertex,
        vs_out[inds[fr.id + 1]].vertex,
        vs_out[inds[fr.id + 2]].vertex
    };

    MyShader::vs_in_t out;
    for(int i = 0; i < sizeof(MyShader::vs_in_t) / sizeof(float); ++i) {
        out.data[i] = a * v[0].data[i] + b * v[1].data[i] + c * v[2].data[i];
    }
    return out;
}

using MyPipeline  = pipeline_t<_NTHREADS,
                                MyShader,
                                WFRasterizer_t,
                                MyInterpolator_t,
                                XWindow>;

std::vector<vec2_t> verts =
{
    {-1.f, -1.f},
    {-1.f,  0.f},
    {-1.f,  1.f},
    { 0.f, -1.f},
    { 0.f,  0.f},
    { 0.f,  1.f},
    { 1.f, -1.f},
    { 1.f,  0.f},
    { 1.f,  1.f}
};
std::vector<unsigned> inds =
{
    0, 3, 1,
    1, 3, 4,
    3, 6, 4,
    4, 6, 7,
    1, 4, 2,
    2, 4, 5,
    4, 7, 5,
    5, 7, 8
};
int main()
{
    //Mouse mouse;
    XWindow window(_HSCRES, _VSCRES);
    unsigned int const width = window.width;
    unsigned int const height = window.height;

    ThreadPool tp(_NTHREADS);
    tp.start();
    float *exdb = new float[width * height];

    MyPipeline pipe(tp, window);
    pipe.init(width, height, exdb);
    pipe.rasterizer.width = width;
    pipe.rasterizer.cullface = WFRasterizer_t::cullface_t::FRONT;
    pipe.rasterizer.height = height;

    int const off = 0;
    pipe.shader.cam = {1.f, static_cast<float>(width) / height, {0.f, 0.f, 0.f}, {0.f, 0.f, 0.f}, {0.f, 0.f, 1.f}};

    pipe.load(verts, inds);
    pipe.vsbatchsz = 128;

    //float phi = 0.f;
    float theta = 0.4f;
    int iter = 0;
    int const iters = 500;
    while(iter < iters)
    {
        window.clear();
        for(int i = 0; i < width * height; ++i) {
            exdb[i] = 2.f;
        }
        pipe.clear();

        float const phi = 2.f / iters * M_PI * iter++;
        vec3_t const dir{
            std::cos(phi) * std::cos(theta),
            std::sin(phi) * std::cos(theta),
            std::sin(theta)
        };

        pipe.shader.cam.pos = dir * 1.5f;

        auto const t0 = std::chrono::system_clock::now();

        pipe();

        auto const t1 = std::chrono::system_clock::now();
        std::chrono::duration<double, std::milli> const dt = t1 - t0;
        std::cout << dt.count() << std::endl;

        window.update();
    }
}
