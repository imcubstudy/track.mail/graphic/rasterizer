#include "WFObj.hpp"

#define _NTHREADS 8

#define _VSCRES 1080
#define _HSCRES 1920

struct SkyShader_t final: shader_t<vertex_t, XWindow::color_t>
{
    mat4_t proj;

    vs_out_t vs(const vs_in_t& vs_in) noexcept override
    {
        return vs_out_t {
            proj * (vec4_t)vs_in.pos,
            vs_in
        };
    }

    PPMTexture_t *objtexture;

    color_t fs(const fs_in_t& fs_in) noexcept override
    {
        vec3_t const c = objtexture->fetch(fs_in.tex);
        return color_t{
            static_cast<unsigned char>(c.z),
            static_cast<unsigned char>(c.y),
            static_cast<unsigned char>(c.x),
            255
        };
    }
};

int main(const int argc, char *const argv[])
{
    XWindow window(_HSCRES, _VSCRES);
    int const width = window.width;
    int const height = window.height;

    ThreadPool tp(_NTHREADS);
    tp.start();

    float *exdb = new float[width * height];

    pipeline_t<
        _NTHREADS,
        WFShader_t,
        WFRasterizer_t,
        WFInterpolator_t,
        XWindow
    >  pipe(tp, window);
    pipeline_t<
        _NTHREADS,
        SkyShader_t,
        WFRasterizer_t,
        WFInterpolator_t,
        XWindow
    >  skypipe(tp, window);

    pipe.init(width, height, exdb);
    pipe.rasterizer.width = width;
    pipe.rasterizer.height = height;

    skypipe.init(width, height, exdb);
    skypipe.rasterizer.width = width;
    skypipe.rasterizer.height = height;

    WFMesh_t sky_mesh = import_obj("A6M/sky.obj");
    for(auto &v: sky_mesh.verts) {
        v.pos = v.pos * 1000.f;
    }
    PPMTexture_t sky_tex = import_texture("A6M/sky.ppm");
    skypipe.shader.objtexture = &sky_tex;
    skypipe.load(sky_mesh.verts, sky_mesh.inds);
    skypipe.vsbatchsz = 128;
    skypipe.rasterizer.cullface = WFRasterizer_t::BACK;

    WFMesh_t a6m_mesh = import_obj("A6M/A6M.obj");
    PPMTexture_t a6m_tex = import_texture("A6M/A6M.ppm");
    pipe.shader.objtexture = &a6m_tex;
    pipe.shader.light = (vec3_t{1.f, 1.f, 3.f}).norm();
    pipe.load(a6m_mesh.verts, a6m_mesh.inds);
    pipe.vsbatchsz = 128;
    pipe.rasterizer.cullface = WFRasterizer_t::FRONT;

    const mat4_t persp_proj = persp(1.f, (float)(width) / height, 0.1f, 1050.f);
    const vec3_t up{0.f, 1.f, 0.f};
    const vec3_t at{0.f, 0.f, 1.f};

    const float theta = 0.4f;
    int iter = 0;
    const int iters = 2000;
    while(iter < iters)
    {
        window.clear();
        for(int i = 0; i < width * height; ++i) {
            exdb[i] = 2.f;
        }
        skypipe.clear();
        pipe.clear();

        const float phi = 2.f / iters * M_PI * iter++;
        vec3_t const dir{
            std::sin(phi) * std::cos(theta),
            std::sin(theta),
            std::cos(phi) * std::cos(theta)
        };

        vec3_t const campos = dir * 10.f;
        mat4_t lookAt = lookat(campos + at, at, up);

        pipe.shader.proj = persp_proj * lookAt;
        pipe.shader.campos = campos;

        skypipe.shader.proj = persp_proj * lookAt;

        auto const t0 = std::chrono::system_clock::now();

        pipe();
        skypipe();

        auto const t1 = std::chrono::system_clock::now();
        std::chrono::duration<double, std::milli> const dt = t1 - t0;
        std::cout << dt.count() << std::endl;

        window.update();
    }
    return 0;
}
