#ifndef __WFOBJ_H__
#define __WFOBJ_H__

#include "Geometry.hpp"
#include "XWindow.hpp"
#include "Pipeline.hpp"
#include <iostream>
#include <fstream>
#include <vector>

union vertex_t
{
    struct
    {
        vec3_t  pos;
        vec2_t  tex;
        vec3_t  norm;
    };
    float data[8];
};

struct WFMesh_t
{
    using uint = unsigned;
    std::vector<vertex_t>   verts;
    std::vector<uint>       inds;
};

WFMesh_t import_obj(char const *filename);

struct PPMTexture_t
{
    unsigned width;
    unsigned height;
    
    std::vector<std::vector<XWindow::color_t>> data;
    vec3_t fetch(const vec2_t& tx);
};

PPMTexture_t import_texture(char const *filename);

WFMesh_t import_obj(char const *filename)
{
    std::ifstream in(filename);
    WFMesh_t out;
    if(!in.is_open())
        assert(!"can not find obj file");
    
    std::vector<vec3_t> pos;
    std::vector<vec2_t> tex;
    std::vector<vec3_t> norm;
    
    std::string line;
    while(std::getline(in, line))
    {
        char const * const cstr = line.c_str();
        if(cstr[0] == 'v')
        {
            vec3_t v;
            vec2_t vt;
            if(sscanf(cstr + 1, " %f %f %f", &v.x, &v.y, &v.z) == 3)
                pos.push_back(v);
            else if(sscanf(cstr + 1, "t %f %f", &vt.x, &vt.y) == 2)
                tex.push_back(vt);
            else if(sscanf(cstr + 1, "n %f %f %f", &v.x, &v.y, &v.z) == 3)
                norm.push_back(v);
        }
        else if(cstr[0] == 'f')
        {
            WFMesh_t::uint idx[3];
            auto const vsize = out.verts.size();
            
            char const *cptr = cstr + 2;
            int eaten;
            while(sscanf(cptr, "%u/%u/%u%n", idx, idx + 1, idx + 2, &eaten) == 3)
            {
                out.verts.push_back({pos[idx[0] - 1], tex[idx[1] - 1], norm[idx[2] - 1]});
                cptr += eaten;
            }
            
            auto const vcount = out.verts.size() - vsize;
            for(auto i = 1u; i < vcount - 1; i++)
            {
                out.inds.push_back((unsigned)(vsize));
                out.inds.push_back((unsigned)(vsize + i));
                out.inds.push_back((unsigned)(vsize + i + 1));
            }
        }
    }
    return out;
}

PPMTexture_t import_texture(char const *filename)
{
    FILE* file = fopen(filename, "rb");
    assert(!"Can't open texture file" || file != NULL);
    
    PPMTexture_t out;
    
    char s[1024] = {};
    fscanf(file, "%s", s);
    assert(!"Invalid file format" || strcmp("P6", s) == 0);
    
    fscanf(file, "%d %d", &out.width, &out.height);
    
    int bpc = 0;
    fscanf(file, "%d%*[ \n]", &bpc);
    assert(!"Invalid bpc" || bpc == 255);
    
    out.data.resize(out.height);
    for(auto &line: out.data) {
        line.resize(out.width);
    }
    
    unsigned char *fbuf = new unsigned char[out.width * out.height * 3];
    fread(fbuf, sizeof(*fbuf), out.width * out.height * 3, file);
    
    for(int y = 0; y < out.height; ++y) {
        for(int x = 0; x < out.width; ++x) {
            out.data[out.height - y - 1][x] = XWindow::color_t {
                .b = fbuf[(y * out.width + x) * 3 + 2],
                .r = fbuf[(y * out.width + x) * 3 + 0],
                .g = fbuf[(y * out.width + x) * 3 + 1],
                .a = 255
            };
        }
    }
    
    free(fbuf);
    fclose(file);
    
    return out;
}

vec3_t PPMTexture_t::fetch(const vec2_t& _tx)
{
    const vec2_t tx = (_tx - vec2_t{0.5f, 0.5f}) * 2;
    const int px = ftop(tx.x, width);
    const int py = ftop(tx.y, height);

    if(py >= 0 && py < height && px >= 0 && px < width) {

        const XWindow::color_t c = data[py][px];

        return vec3_t{
            (float) c.r,
            (float) c.g,
            (float) c.b
        };
    }
    return vec3_t{0.f, 0.f, 0.f};
}

#include "WFPipeline.hpp"

#endif // __WFOBJ_H__
