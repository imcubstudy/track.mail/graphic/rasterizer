#ifndef __WFPIPELINE_HPP__
#define __WFPIPELINE_HPP__

struct fragment_t
{
    int x, id;
    float b, c;
    float depth;
};

struct WFShader_t final: shader_t<vertex_t, XWindow::color_t>
{
    vec3_t light;
    vec3_t campos;

    PPMTexture_t *objtexture;

    mat4_t proj;

    vs_out_t vs(const vs_in_t& vs_in) noexcept override;
    color_t  fs(const fs_in_t& fs_in) noexcept override;
};

struct WFRasterizer_t final: rasterizer_t<3, fragment_t>
{
    int width;
    int height;

    enum cullface_t { NONE, FRONT, BACK };

    cullface_t cullface = NONE;

    struct line_t
    {
        const vec2_t a, b, d;
        float t;
        line_t() = default;
        line_t(const vec3_t& a, const vec3_t& b);

        float getx(const float& y) const noexcept;

        bool inside(const float& y) const noexcept;
    };

    void operator()(vec4_t inp[3],
                    std::vector<std::vector<fragment_t>>& fl,
                    std::vector<unsigned>& fls,
                    const float *exdepth,
                    unsigned id) noexcept override;
};

inline WFShader_t::vs_out_t WFShader_t::vs(const WFShader_t::vs_in_t& vs_in) noexcept
{
    return vs_out_t {
        proj * (vec4_t)vs_in.pos,
        vs_in
    };
}

inline WFRasterizer_t::line_t::line_t(const vec3_t& a, const vec3_t& b):
    a({a.x, a.y}),
    b({b.x, b.y}),
    d({b.x - a.x, b.y - a.y}),
    t((b.x - a.x) / (b.y - a.y))
{
    if((b.y - a.y) == 0) {
        t = 0;
    }
}

inline float WFRasterizer_t::line_t::getx(const float& y) const noexcept
{
    return a.x + t * (y - a.y);
}

inline bool WFRasterizer_t::line_t::inside(const float& y) const noexcept
{
    float p = (y - a.y) / d.y;
    return 0 != d.y && p >= 0 && p <= 1;
}

inline void WFRasterizer_t::operator()(vec4_t inp[3],
                                       std::vector<std::vector<fragment_t>>& fl,
                                       std::vector<unsigned>& fls,
                                       const float *exdepth,
                                       unsigned id) noexcept
{
    if(inp[0].w >= 0 || inp[1].w >= 0 || inp[1].w >= 0) {
        return;
    }

    vec3_t mapped[3] = {
        (vec3_t)inp[0],
        (vec3_t)inp[1],
        (vec3_t)inp[2]
    };

    const float _dx1 = mapped[1].x - mapped[0].x;
    const float _dx2 = mapped[2].x - mapped[0].x;

    const float _dy1 = mapped[1].y - mapped[0].y;
    const float _dy2 = mapped[2].y - mapped[0].y;

    const float det = _dx1 * _dy2 - _dx2 * _dy1;

    if(cullface != NONE) {
        if((cullface == FRONT && det < 0) ||
           (cullface == BACK  && det > 0))
        {
            return;
        }
    }

    const int bbx_max = std::min(ftop(std::max(mapped[0].x, std::max(mapped[1].x, mapped[2].x)), width), width - 1);
    const int bbx_min = std::max(ftop(std::min(mapped[0].x, std::min(mapped[1].x, mapped[2].x)), width), 0);

    const int bby_max = std::min(ftop(std::max(mapped[0].y, std::max(mapped[1].y, mapped[2].y)), height), height - 1);
    const int bby_min = std::max(ftop(std::min(mapped[0].y, std::min(mapped[1].y, mapped[2].y)), height), 0);

    if(bbx_max - bbx_min < 1 || bby_max - bby_min < 1) {
        return;
    }

    const vec3_t invwvec = {1 / inp[0].w, 1 / inp[1].w, 1 / inp[2].w};
    const vec3_t depthvec = {mapped[0].z, mapped[1].z, mapped[2].z};

    const float dx1 = _dx1 / det;
    const float dx2 = _dx2 / det;
    const float dy1 = _dy1 / det;
    const float dy2 = _dy2 / det;

    line_t l[3]  = {
        {mapped[0], mapped[1]},
        {mapped[0], mapped[2]},
        {mapped[1], mapped[2]}
    };

    fragment_t out = {};
    out.id = id;

    for(int y = bby_min; y <= bby_max; ++y) {
        const float fy = ptof(y, height);
        const float dy = fy - mapped[0].y;

        const float xv[3] = {
            l[0].getx(fy),
            l[1].getx(fy),
            l[2].getx(fy)
        };

        float fx_min = -1.f;
        float fx_max = +1.f;

        bool wasinside = false;
        for(int i = 0; i < 3; ++i) {
            if(l[i].inside(fy)) {
                wasinside = true;
                fx_min = std::max(fx_min, xv[i]);
                fx_max = std::min(fx_max, xv[i]);
            }
        }
        if(!wasinside) continue;

        std::swap(fx_max, fx_min);
        const int x_max = std::min(bbx_max, ftop(fx_max, width));
        const int x_min = std::max(bbx_min, ftop(fx_min, width));

        for(int x = x_min; x <= x_max; ++x) {
            const float fx = ptof(x, width);
            const float dx = fx - mapped[0].x;

            const float b0 = dx * dy2 - dy * dx2;
            const float c0 = dx1 * dy - dy1 * dx;
            const float a0 = 1.f - b0 - c0;
            if(a0 <= 0 || b0 <= 0 || c0 <= 0) {
                continue;
            }

            const float depth = depthvec[0] * a0
                              + depthvec[1] * b0
                              + depthvec[2] * c0;
                              
            if(!(depth > -1 && depth < 1 && depth < exdepth[y * width + x])) {
                continue;
            }

            const float a = a0 * invwvec[0];
            const float b = b0 * invwvec[1];
            const float c = c0 * invwvec[2];
            const float sum = a + b + c;

            out.x = x;
            out.b = b / sum;
            out.c = c / sum;
            out.depth = depth;

            if(fls[y] < fl[y].size()) {
                fl[y][fls[y]] = out;
            } else {
                fl[y].push_back(out);
            }
            fls[y]++;
        }
    }
}

struct WFInterpolator_t final: interpolator_t<WFShader_t::fs_in_t, WFShader_t::vs_out_t, fragment_t>
{
    fs_in_t operator()(const std::vector<vs_out_t>& vs_out, const std::vector<unsigned>& inds, const fragment_t& fr) noexcept override;
};

inline vertex_t WFInterpolator_t::operator()(const std::vector<WFInterpolator_t::vs_out_t>& vs_out,
                                             const std::vector<unsigned>& inds, const fragment_t& fr) noexcept
{
    const float b = fr.b;
    const float c = fr.c;
    const float a = 1 - b - c;

    const vertex_t v[3] = {
        vs_out[inds[fr.id + 0]].vertex,
        vs_out[inds[fr.id + 1]].vertex,
        vs_out[inds[fr.id + 2]].vertex
    };

    vertex_t out;
    for(int i = 0; i < sizeof(vertex_t) / sizeof(float); ++i) {
        out.data[i] = a * v[0].data[i] + b * v[1].data[i] + c * v[2].data[i];
    }
    return out;
}

inline WFShader_t::color_t WFShader_t::fs(const WFShader_t::fs_in_t& fs_in) noexcept
{
    vec3_t const camera = (campos - fs_in.pos).norm();
    vec3_t const halfway = (light + camera).norm();
    float const NH = std::max(0.f, fs_in.norm.dot(halfway));
    float spec = NH;
    for(int i = 0; i < 4; i++) {
        spec *= spec;
    }
    float const diff = std::max(0.f, fs_in.norm.dot(light));
    vec3_t const c = objtexture->fetch(fs_in.tex) * (0.2f + 0.4f * diff + 0.4f * spec);
    return color_t{
        static_cast<unsigned char>(c.z),
        static_cast<unsigned char>(c.y),
        static_cast<unsigned char>(c.x),
        255
    };
}

#endif // __WFPIPELINE_HPP__
