#pragma once
#include "Geometry.hpp"

struct sphere
{
    vec3_t pos;
    float R;

    vec3_t color;
};

struct ray
{
    vec3_t pos;
    vec3_t dir;
};

inline vec3_t ray_vec(ray const &r, float const t) noexcept
{
    return r.pos + r.dir * t;
}

inline float intersect_sphere(sphere const &s, ray const &r) noexcept
{
    vec3_t const rp = r.pos - s.pos;
    float const ra_rp = r.dir.dot(rp);
    if(ra_rp > 0.f)
        return 0.f;
    float const d = ra_rp * ra_rp - rp.dot(rp) + s.R * s.R;
    if(d < 0.f)
        return 0.f;
    return -ra_rp - sqrtf(d);
}

struct camera
{
    float fov;
    float ratio;
    vec3_t pos;
    vec3_t at;
    vec3_t up;
};

inline ray cast_from_cam(camera const &cam, vec2_t const &r) noexcept
{
    vec3_t const n = (cam.at - cam.pos).norm();
    vec3_t const right = n.cross(cam.up);
    vec3_t const up = right.cross(n);
    vec3_t const x = right.norm() * (cam.fov * r.x * cam.ratio);
    vec3_t const y = up.norm() * (cam.fov * r.y);
    return ray
    {
        cam.pos,
        (n + x + y).norm()
    };
}
struct intersection
{
    float t;
    int idx;
};
inline intersection find_intersection(ray const &r, sphere const *sph, int sph_sz) noexcept
{
    float depth = 1e6;
    float const dmin = 1e-3;
    int idx = sph_sz;
    for(int i = 0; i < sph_sz; i++)
    {
        float const d = intersect_sphere(sph[i], r);
        if(d > dmin && d < depth)
        {
            depth = d;
            idx = i;
        }
    }
    return intersection{depth, idx == sph_sz ? -1 : idx};
}
