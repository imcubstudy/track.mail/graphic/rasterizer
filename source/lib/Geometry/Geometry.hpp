#ifndef __GEOMETRY_H__
#define __GEOMETRY_H__

#include <cmath>

union vec2_t
{
    float data[2];
    struct
    {
        float x;
        float y;
    };

    vec2_t norm() const noexcept;

    float dot(const vec2_t &rhs) const noexcept;

    vec2_t operator+(const vec2_t &rhs) const noexcept;
    vec2_t operator-(const vec2_t &rhs) const noexcept;
    vec2_t operator/(const float &rhs)  const noexcept;
    vec2_t operator*(const float &rhs)  const noexcept;
    friend vec2_t operator*(const float &lhs, const vec2_t &rhs) noexcept;

    float&          operator[](int idx)             noexcept;
    const float&    operator[](int idx) const       noexcept;
};

union vec4_t;

union vec3_t
{
    float data[3];
    struct
    {
        float x;
        float y;
        float z;
    };

    vec3_t norm() const noexcept;

    float  dot(const vec3_t &rhs)   const noexcept;
    vec3_t cross(const vec3_t &rhs) const noexcept;

    vec3_t operator+(const vec3_t &rhs) const noexcept;
    vec3_t operator-(const vec3_t &rhs) const noexcept;
    vec3_t operator/(const float &rhs)  const noexcept;
    vec3_t operator*(const float &rhs)  const noexcept;
    friend vec3_t operator*(const float &lhs, const vec3_t &rhs) noexcept;

    float&          operator[](int idx)             noexcept;
    const float&    operator[](int idx) const       noexcept;

    explicit operator vec4_t() const noexcept;
};

union vec4_t
{
    float data[4];
    struct
    {
        float x;
        float y;
        float z;
        float w;
    };

    vec4_t norm() const noexcept;

    float dot(const vec4_t &rhs) const noexcept;

    vec4_t operator+(const vec4_t &rhs) const noexcept;
    vec4_t operator-(const vec4_t &rhs) const noexcept;
    vec4_t operator/(const float &rhs)  const noexcept;
    vec4_t operator*(const float &rhs)  const noexcept;
    friend vec4_t operator*(const float &lhs, const vec4_t &rhs) noexcept;

    float&          operator[](int idx)             noexcept;
    const float&    operator[](int idx) const       noexcept;

    explicit operator vec3_t() const noexcept;
};

float magn(const vec2_t &v) noexcept;
float magn(const vec3_t &v) noexcept;
float magn(const vec4_t &v) noexcept;

struct mat4_t
{
    float data[4][4];

    mat4_t operator*(const mat4_t &rhs) const noexcept;
    vec4_t operator*(const vec4_t &rhs) const noexcept;

    float       *operator[](int idx)        noexcept;
    float const *operator[](int idx) const  noexcept;
};

#define $norm(N)                                                                \
    vec##N##_t vec##N##_t::norm() const noexcept                                \
    {                                                                           \
        float _magn = magn(*this);                                              \
        return (*this) * (1 / _magn);                                           \
    }

$norm(2)
$norm(3)
$norm(4)

#undef $norm

#define $magn(N)                                                                \
    float magn(const vec##N##_t &v) noexcept                                    \
    {                                                                           \
        float out = 0;                                                          \
        for(int i = 0; i < N; ++i) {                                            \
            out += v.data[i] * v.data[i];                                       \
        }                                                                       \
        return sqrtf(out);                                                      \
    }

$magn(2)
$magn(3)
$magn(4)

#undef $magn

#define $dot(N)                                                                 \
    float vec##N##_t::dot(const vec##N##_t &rhs) const noexcept                 \
    {                                                                           \
        float out = 0;                                                          \
        for(int i = 0; i < N; ++i) {                                            \
            out += data[i] * rhs.data[i];                                       \
        }                                                                       \
        return out;                                                             \
    }

$dot(2)
$dot(3)
$dot(4)

#undef $dot

#define $operator_plus(N)                                                       \
    vec##N##_t vec##N##_t::operator+(const vec##N##_t &rhs) const noexcept      \
    {                                                                           \
        vec##N##_t out = {};                                                    \
        for(int i = 0; i < N; ++i) {                                            \
            out.data[i] = data[i] + rhs.data[i];                                \
        }                                                                       \
        return out;                                                             \
    }

$operator_plus(2)
$operator_plus(3)
$operator_plus(4)

#undef $operator_plus

#define $operator_minus(N)                                                      \
    vec##N##_t vec##N##_t::operator-(const vec##N##_t &rhs) const noexcept      \
    {                                                                           \
        vec##N##_t out = {};                                                    \
        for(int i = 0; i < N; ++i) {                                            \
            out.data[i] = data[i] - rhs.data[i];                                \
        }                                                                       \
        return out;                                                             \
    }

$operator_minus(2)
$operator_minus(3)
$operator_minus(4)

#undef $operator_minus

#define $operator_mul(N)                                                        \
    vec##N##_t vec##N##_t::operator*(const float &rhs) const noexcept           \
    {                                                                           \
        vec##N##_t out = {};                                                    \
        for(int i = 0; i < N; ++i) {                                            \
            out.data[i] = data[i] * rhs;                                        \
        }                                                                       \
        return out;                                                             \
    }                                                                           \
                                                                                \
    vec##N##_t operator*(const float &lhs, const vec##N##_t &rhs) noexcept      \
    {                                                                           \
        return rhs * lhs;                                                       \
    }                                                                           \

$operator_mul(2)
$operator_mul(3)
$operator_mul(4)

#undef $operator_mul

#define $operator_div(N)                                                        \
    vec##N##_t vec##N##_t::operator/(const float &rhs) const noexcept           \
    {                                                                           \
        vec##N##_t out = {};                                                    \
        for(int i = 0; i < N; ++i) {                                            \
            out.data[i] = data[i] / rhs;                                        \
        }                                                                       \
        return out;                                                             \
    }

$operator_div(2)
$operator_div(3)
$operator_div(4)

#undef $operator_div

#define $operator_sqrbr(N)                                                      \
    float& vec##N##_t::operator[](int idx) noexcept                             \
    { return data[idx]; }                                                       \
                                                                                \
    const float& vec##N##_t::operator[](int idx) const noexcept                 \
    { return data[idx]; }

$operator_sqrbr(2)
$operator_sqrbr(3)
$operator_sqrbr(4)

#undef $operator_sqrbr

vec3_t vec3_t::cross(const vec3_t &rhs) const noexcept
{
    return vec3_t{
        -z * rhs.y + y * rhs.z,
        -x * rhs.z + z * rhs.x,
        -y * rhs.x + x * rhs.y
    };
}

vec3_t::operator vec4_t() const noexcept
{
    return vec4_t{
        x,
        y,
        z,
        1
    };
}

vec4_t::operator vec3_t() const noexcept
{
    return vec3_t{
        x / w,
        y / w,
        z / w
    };
}

mat4_t mat4_t::operator*(const mat4_t &rhs) const noexcept
{
    mat4_t out = {};
    for(int i = 0; i < 4; ++i) {
        for(int j = 0; j < 4; ++j) {
            for(int k = 0; k < 4; ++k) {
                out.data[i][j] += data[i][k] * rhs.data[k][j];
            }
        }
    }
    return out;
}

vec4_t mat4_t::operator*(const vec4_t &rhs) const noexcept
{
    vec4_t out = {};
    for(int i = 0; i < 4; ++i) {
        for(int j = 0; j < 4; ++j) {
            out.data[i] += data[i][j] * rhs.data[j];
        }
    }
    return out;
}

float *mat4_t::operator[](int idx) noexcept
{
    return data[idx];
}

float const *mat4_t::operator[](int idx) const noexcept
{
    return data[idx];
}

mat4_t persp(float fov, float ratio, float znear, float zfar) noexcept
{
    return mat4_t{
        -1.f / std::tan(fov), 0.f, 0.f, 0.f,
        0.f, -ratio / std::tan(fov), 0.f, 0.f,
        0.f, 0.f, (zfar + znear) / (zfar - znear), 2.f * znear * zfar / (zfar - znear),
        0.f, 0.f, 1.f, 0.f
    };
}
mat4_t lookat(vec3_t eye, vec3_t at, vec3_t up) noexcept
{
    vec3_t const dir = (eye - at).norm();
    vec3_t const right = (up.cross(dir)).norm();
    up = (dir.cross(right)).norm();
    return mat4_t{
        right.x, right.y, right.z, -eye.dot(right),
        up.x,    up.y,    up.z,    -eye.dot(up),
        dir.x,   dir.y,   dir.z,   -eye.dot(dir),
        0.f,     0.f,     0.f,     1.f
    };
}

#endif // __GEOMETRY_H__
