#ifndef __XWINDOW_HPP__
#define __XWINDOW_HPP__

#include <stdexcept>
#include <cmath>
#include <cstring>

#include "Pipeline.hpp"

extern "C"
{
    #include <X11/Xlib.h>
    #include <X11/Xutil.h>
    #include <X11/Xos.h>
    #include <X11/Xatom.h>
}

struct X11color_t
{
    unsigned char b, r, g, a;
};

struct XWindow final: public canvas_t<X11color_t>
{
public:

    ~XWindow();
    XWindow(const int& w, const int& h);

    XWindow(XWindow const &) = delete;
    XWindow(XWindow &&) = delete;

    int width;
    int height;

    void update()               noexcept;
    void update(color_t *fb)    noexcept;
    void clear()                noexcept;

    color_t *memory() noexcept;

    color_t *operator[](int y) noexcept override;

private:
    Display    *display;
    int         screen;
    Window      window;

    GC          gc;
    XImage     *image;
    color_t    *pixels;
};

void XWindow::update() noexcept
{
    XPutImage(display, window, gc, image, 0, 0, 0, 0, width, height);
}

void XWindow::update(color_t *fb) noexcept
{
    std::memcpy(pixels, fb, width * height * sizeof(color_t));
    update();
}

void XWindow::clear() noexcept
{
    wmemset(reinterpret_cast<wchar_t *>(pixels), 0xff000000, width * height);
}

XWindow::color_t *XWindow::memory() noexcept
{
    return pixels;
}

XWindow::color_t *XWindow::operator[](int const i) noexcept
{
    return pixels + (height - i - 1) * width;
}

XWindow::XWindow(const int &w, const int& h)
{
    this->width = w;
    this->height = h;

    display = XOpenDisplay(getenv("DISPLAY"));
    if(display == NULL)
        assert(!"could not open x11 display");
    screen = DefaultScreen(display);
    window = XCreateSimpleWindow
    (
        display,
        DefaultRootWindow(display),
        0,
        0,
        this->width,
        this->height,
        0,
        WhitePixel(display, screen),
        BlackPixel(display, screen)
    );

    pixels = (color_t*)std::calloc(this->width * this->height, sizeof(color_t));

    gc = XCreateGC(display, window, 0, 0);

    XSelectInput(display, window, PointerMotionMask);
    image = XCreateImage
    (
        display,
        DefaultVisual(display, screen),
        DefaultDepth(display, screen),
        ZPixmap,
        0,
        reinterpret_cast<char *>(pixels),
        this->width,
        this->height,
        32,
        0
    );
    XClearWindow(display, window);
    XMapRaised(display, window);
}

XWindow::~XWindow()
{
    XDestroyImage(image);
    XFreeGC(display, gc);
    XDestroyWindow(display, window);
    XCloseDisplay(display);
}

#endif // __XWINDOW_HPP__
