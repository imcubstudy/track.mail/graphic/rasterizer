#include "Mouse.h"

#include <fcntl.h>
#include <unistd.h>
#include <cassert>

Mouse::Mouse():
    mouse_fd(::open("/dev/input/mice", O_RDONLY | O_NONBLOCK))
{
    assert(!"/dev/input/mice" && mouse_fd > 0);
}

Mouse::~Mouse() noexcept
{
    ::close(mouse_fd);
}

bool Mouse::poll(Mouse::event_t &e) noexcept
{
    return ::read(mouse_fd, &e, sizeof(e)) > 0;
}

bool Mouse::event_t::left_button() const noexcept
{
    return (flags & 0x1) != 0;
}

bool Mouse::event_t::middle_button() const noexcept
{
    return (flags & 0x2) != 0;
}

bool Mouse::event_t::right_button() const noexcept
{
    return (flags & 0x4) != 0;
}
