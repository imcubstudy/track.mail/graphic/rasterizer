class Mouse
{
public:
    struct event_t
    {
        char flags;
        char dx;
        char dy;

        bool   left_button() const noexcept;
        bool  right_button() const noexcept;
        bool middle_button() const noexcept;
    };

    ~Mouse() noexcept;
    Mouse();

    bool poll(event_t &e) noexcept;

private:
    int const mouse_fd;
};
