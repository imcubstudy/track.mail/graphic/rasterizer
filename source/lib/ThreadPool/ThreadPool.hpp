#ifndef __THREADPOOL_H__
#define __THREADPOOL_H__

#include <functional>
#include <thread>
#include <vector>
#include <deque>
#include <mutex>
#include <atomic>
#include <condition_variable>

class ThreadPool
{
private:
    std::vector<std::thread>            _thread;

    std::deque<std::function<void()>>   _task;

    std::mutex                          _mutex;

    std::condition_variable             _cv_task;
    std::condition_variable             _cv_finish;

    bool                                _finish;

    unsigned int                        _busy;
    std::atomic_uint                    _done;

    void routine();

public:
    const int concurrensy;

    ThreadPool(const int& N) noexcept;

    void start() noexcept;

    ~ThreadPool() noexcept;

    template <typename F>
    void enqueue(F&& f) noexcept;

    void wait() noexcept;

    unsigned int done() noexcept;
};

template <typename F>
inline void ThreadPool::enqueue(F&& f) noexcept
{
    std::unique_lock<std::mutex> latch(_mutex);
    _task.emplace_back(std::forward<F>(f));
    _cv_task.notify_one();
}

inline ThreadPool::ThreadPool(const int& N) noexcept:
    concurrensy(N),
    _finish(false),
    _busy(0),
    _done(0)
{}

inline void ThreadPool::start() noexcept
{
    for(int i = 0; i < concurrensy; ++i) {
        _thread.emplace_back(std::thread(std::bind(&ThreadPool::routine, this)));
    }
}

inline ThreadPool::~ThreadPool() noexcept
{
    std::unique_lock<std::mutex> latch(_mutex);
    _finish = true;
    _cv_task.notify_all();
    latch.unlock();

    for(auto &t: _thread) {
        t.join();
    }
}

inline void ThreadPool::routine()
{
    while(true) {
        std::unique_lock<std::mutex> latch(_mutex);
        _cv_task.wait(latch, [this]() -> bool {
            return _finish || !_task.empty();
        });
        if(!_task.empty()) {
            _busy++;

            auto task = _task.front();
            _task.pop_front();

            latch.unlock();

            task();
            _done++;

            latch.lock();
            _busy--;
            _cv_finish.notify_one();

        } else if(_finish) {
            break;
        }
    }
}

inline void ThreadPool::wait() noexcept
{
    std::unique_lock<std::mutex> latch(_mutex);
    _cv_finish.wait(latch, [this](){ return _task.empty() && (_busy == 0); });
}

inline unsigned int ThreadPool::done() noexcept
{
    unsigned out = _done;
    _done = 0;
    return out;
}

#endif // __THREADPOOL_H__
