#ifndef __CLASSES_HPP__
#define __CLASSES_HPP__

template <
    typename _vertex_t,
    typename _color_t
>
struct shader_t
{
    using vs_in_t   = _vertex_t;
    using vs_out_t  = struct {
        vec4_t      vec4;
        _vertex_t   vertex;
    };
    using fs_in_t = _vertex_t;
    using color_t = _color_t;

    virtual vs_out_t vs(const vs_in_t& vs_in) noexcept = 0;
    virtual color_t  fs(const fs_in_t& fs_in) noexcept = 0;
};

template <
    int         _inpsz,
    typename    _fragment_t
>
struct rasterizer_t
{
    static const int inpsz  = _inpsz;
    using fragment_t        = _fragment_t;

    virtual void operator()(vec4_t inp[_inpsz],
                            std::vector<std::vector<_fragment_t>>& fl,
                            std::vector<unsigned>& fls,
                            const float *exdepth,
                            unsigned id) noexcept = 0;
};

template <
    typename    _fs_in_t,
    typename    _vs_out_t,
    typename    _fragment_t
>
struct interpolator_t
{
    using fs_in_t       = _fs_in_t;
    using vs_out_t      = _vs_out_t;
    using fragment_t    = _fragment_t;

    virtual fs_in_t operator()(const std::vector<vs_out_t>& vs_out,
                               const std::vector<unsigned>& inds,
                               const fragment_t& fr) noexcept = 0;
};

template <typename _color_t>
struct canvas_t
{
    using color_t = _color_t;

    virtual color_t* operator[](int idx) noexcept = 0;
};

#endif // __CLASSES_HPP__
