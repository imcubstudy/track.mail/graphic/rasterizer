#ifndef __PIPELINE_H__
#define __PIPELINE_H__

#include "ThreadPool.hpp"
#include "Geometry.hpp"
#include <vector>
#include <algorithm>
#include <cassert>
#include "Classes.hpp"

inline int ftop(float f, int sz) noexcept
{
    return (int)(roundf(-0.5f + sz / 2.f * (f + 1)));
}

inline float ptof(int p, int sz) noexcept
{
    return (-1 + (2 * p + 1.f) / sz);
}

#define _pipeline_t_template template <int      _NThreads,                      \
                                       typename _shader_t,                      \
                                       typename _rasterizer_t,                  \
                                       typename _interpolator_t,                \
                                       typename _canvas_t>                      \

#define _pipeline_t pipeline_t<_NThreads,                                       \
                               _shader_t,                                       \
                               _rasterizer_t,                                   \
                               _interpolator_t,                                 \
                               _canvas_t>                                       \

_pipeline_t_template
struct pipeline_t
{
    ThreadPool&         threadpool;
    _canvas_t&          canvas;

    _shader_t           shader;
    _rasterizer_t       rasterizer;
    _interpolator_t     interpolator;

    int width;
    int height;

    using vs_in_t    = typename _shader_t::vs_in_t;
    using vs_out_t   = typename _shader_t::vs_out_t;

    pipeline_t(ThreadPool& tp, _canvas_t& canvas) noexcept;

    void init(int w, int h, float* exdb) noexcept;

    void load(const std::vector<vs_in_t>& vs_in, const std::vector<unsigned>& inds) noexcept;

    void operator()() noexcept;

    std::atomic_uint        atom;

    // clear ###################################################################
    struct clear_task_t
    {
        pipeline_t *p;
        void operator()() noexcept;
    };
    void clear() noexcept;
    // clear ###################################################################

    // vertex shader ###########################################################
    std::vector<vs_in_t>    vs_in;
    std::vector<unsigned>   inds;
    std::vector<vs_out_t>   vs_out;
    unsigned int            vsbatchsz;

    struct vs_task_t
    {
        pipeline_t  *p;
        void operator()() noexcept;
    };
    // vertex shader ###########################################################

    // rasterizer ##############################################################
    using fragment_t        = typename _rasterizer_t::fragment_t;
    using fragment_list_t   = std::vector<std::vector<fragment_t>>;

    std::vector<fragment_list_t>        fragment_list;
    std::vector<std::vector<unsigned>>  fls;

    fragment_list_t                     fragments;
    float*                              exdepth;

    struct rast_task_t
    {
        pipeline_t  *p;
        int         tid;
        void operator()() noexcept;
    };
    // rasterizer ##############################################################

    // merge ###################################################################
    struct mergefl_task_t
    {
        pipeline_t  *p;
        void operator()() noexcept;
    };
    // merge ###################################################################

    // fragment shader #########################################################
    using fs_in_t   = typename _shader_t::fs_in_t;

    struct fs_task_t
    {
        pipeline_t  *p;
        void operator()() noexcept;
    };
    // fragment shader #########################################################
};

_pipeline_t_template
inline _pipeline_t::pipeline_t(ThreadPool& tp, _canvas_t& canvas) noexcept:
    threadpool(tp),
    canvas(canvas)
{
    assert(tp.concurrensy == _NThreads);
}

_pipeline_t_template
inline void _pipeline_t::init(int w, int h, float* exdb) noexcept
{
    width = w;
    height = h;

    exdepth = exdb;

    fragment_list.resize(_NThreads);
    for(auto &fl: fragment_list) {
        fl.resize(h);
    }

    fls.resize(_NThreads);
    for(auto &flsi: fls) {
        flsi.resize(h);
        for(auto &i: flsi) {
            i = 0;
        }
    }

    fragments.resize(h);
    for(auto &l: fragments) {
        l.resize(w);
    }
}

_pipeline_t_template
inline void _pipeline_t::load(const std::vector<vs_in_t>& vs_in,
                              const std::vector<unsigned>& inds) noexcept
{
    this->vs_in = vs_in;
    this->inds = inds;
    this->vs_out.resize(vs_in.size());
}

_pipeline_t_template
inline void _pipeline_t::clear() noexcept
{
    atom.store(0);
    for(int i = 0; i < _NThreads; ++i) {
        threadpool.enqueue(clear_task_t{this});
    }
    threadpool.wait();
}

_pipeline_t_template
inline void _pipeline_t::operator()() noexcept
{
    atom.store(0);
    for(int i = 0; i < _NThreads; ++i) {
        threadpool.enqueue(vs_task_t{this});
    }
    threadpool.wait();

    atom.store(0);
    for(int i = 0; i < _NThreads; ++i) {
        threadpool.enqueue(rast_task_t{this, i});
    }
    threadpool.wait();

    atom.store(0);
    for(int i = 0; i < _NThreads; ++i) {
        threadpool.enqueue(mergefl_task_t{this});
    }
    threadpool.wait();

    atom.store(0);
    for(int i = 0; i < _NThreads; ++i) {
        threadpool.enqueue(fs_task_t{this});
    }
    threadpool.wait();
}

_pipeline_t_template
inline void _pipeline_t::vs_task_t::operator()() noexcept
{ do {
    unsigned start = p->atom.fetch_add(1) * p->vsbatchsz;
    if(start >= p->vs_in.size()) {
        break;
    }

    const auto stop = std::min((size_t)start + p->vsbatchsz,
                               p->vs_in.size());
    for(auto i = start; i < stop; ++i) {
        p->vs_out[i] = p->shader.vs(p->vs_in[i]);
    }
} while(true); }

_pipeline_t_template
inline void _pipeline_t::clear_task_t::operator()() noexcept
{ do {
    unsigned line = p->atom.fetch_add(1);
    if(line >= p->height) {
        break;
    }

    for(int x = 0; x < p->width; ++x) {
        p->fragments[line][x].depth = 2;
    }

    for(int t = 0; t < _NThreads; ++t) {
        p->fls[t][line] = 0;
    }
} while(true); }

_pipeline_t_template
inline void _pipeline_t::rast_task_t::operator()() noexcept
{ do {
    unsigned start = p->atom.fetch_add(1) * (p->rasterizer.inpsz);
    if(start >= p->inds.size()) {
        break;
    }

    vec4_t inp[p->rasterizer.inpsz];
    for(int i = 0; i < p->rasterizer.inpsz; ++i) {
        inp[i] = p->vs_out[p->inds[start + i]].vec4;
    }
    p->rasterizer(inp, p->fragment_list[tid], p->fls[tid], p->exdepth, start);
} while(true); }

_pipeline_t_template
inline void _pipeline_t::mergefl_task_t::operator()() noexcept
{ do {
    unsigned line = p->atom.fetch_add(1);
    if(line >= p->canvas.height) {
        break;
    }

    for(int t = 0; t < _NThreads; ++t) {
        for(int i = 0; i < p->fls[t][line]; ++i) {
            fragment_t f = p->fragment_list[t][line][i];
            if(!(f.depth > -1.f && f.depth < 1.f)
              || f.depth > p->fragments[line][f.x].depth) {
                continue;
            }
            p->exdepth[line * p->width + f.x] = f.depth;
            p->fragments[line][f.x] = f;
        }
    }
} while(true); }

_pipeline_t_template
inline void _pipeline_t::fs_task_t::operator()() noexcept
{ do {
    unsigned line = p->atom.fetch_add(1);
    if(line >= p->canvas.height) {
        break;
    }
    for(int x = 0; x < p->width; ++x) {
        if(p->fragments[line][x].depth < 1.f) {
            fs_in_t fs_in = p->interpolator(p->vs_out, p->inds, p->fragments[line][x]);
            p->canvas[line][x] = p->shader.fs(fs_in);
        }
    }
} while(true); }

#undef _pipeline_t_template
#undef _pipeline_t

#endif // __PIPELINE_H__
