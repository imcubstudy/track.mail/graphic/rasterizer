#include "WFObj.hpp"

#define _NTHREADS 8

#define _VSCRES 1080
#define _HSCRES 1920

int main(const int argc, char *const argv[])
{
    XWindow window(_HSCRES, _VSCRES);
    int const width = window.width;
    int const height = window.height;

    ThreadPool tp(_NTHREADS);
    tp.start();

    float *exdb = new float[width * height];

    pipeline_t<
        _NTHREADS,
        WFShader_t,
        WFRasterizer_t,
        WFInterpolator_t,
        XWindow
    >  pipe(tp, window);

    pipe.init(width, height, exdb);
    pipe.rasterizer.width = width;
    pipe.rasterizer.height = height;

    WFMesh_t cat_mesh = import_obj("cat/cat.obj");
    for(auto &v: cat_mesh.verts) {
        v.pos = (v.pos + vec3_t{0.f, -1.f, 0.f}) * 1.5;
    }
    PPMTexture_t cat_tex = import_texture("cat/cat.ppm");
    pipe.shader.objtexture = &cat_tex;
    pipe.shader.light = (vec3_t{1.f, 1.f, 3.f}).norm();
    pipe.load(cat_mesh.verts, cat_mesh.inds);
    pipe.vsbatchsz = 128;
    pipe.rasterizer.cullface = WFRasterizer_t::BACK;

    const mat4_t persp_proj = persp(1.f, (float)(width) / height, 0.1f, 1050.f);
    const vec3_t up{0.f, 1.f, 0.f};
    const vec3_t at{0.f, 0.f, 0.f};

    const float theta = 0.4f;
    int iter = 0;
    const int iters = 2000;
    while(iter < iters)
    {
        window.clear();
        for(int i = 0; i < width * height; ++i) {
            exdb[i] = 2.f;
        }
        pipe.clear();

        const float phi = 2.f / iters * M_PI * iter++;
        vec3_t const dir{
            std::sin(phi) * std::cos(theta),
            std::sin(theta),
            std::cos(phi) * std::cos(theta)
        };

        vec3_t const campos = dir * 3.f;
        mat4_t lookAt = lookat(campos + at, at, up);

        pipe.shader.proj = persp_proj * lookAt;
        pipe.shader.campos = campos;

        auto const t0 = std::chrono::system_clock::now();

        pipe();

        auto const t1 = std::chrono::system_clock::now();
        std::chrono::duration<double, std::milli> const dt = t1 - t0;
        std::cout << dt.count() << std::endl;

        window.update();
    }
    return 0;
}
